Sixty-Two
=========

> An FX viewer in 200 lines of Rust

![Screenshot](/screenshot.png)

See [Writing an FX viewer in 200 lines of Rust][post-62] for a
line-by-line breakdown of this.

[post-62]: https://scvalex.net/posts/62/


## How to build and run

Sixty-Two is just a Rust app, so the usual `cargo` commands work.
However, the app has dependencies on some graphics libraries that
aren't managed by Cargo.

### Manually

First, make sure you have the following dependencies installed:

- Debian: `build-essential`, `libxcb1-dev`, `libxcb-render0-dev`,
  `libxcb-shape0-dev`, and `libxcb-xfixes0-dev`.
- Fedora: `libxcb-devel`
- NixOS: see below

Then, use the usual Rust build commands:

```
$ cargo build
$ cargo run
```

### NixOS

There's a `flake.nix` that takes care of the dependencies.  Run `nix
develop` in the repo to enter a shell with everything setup.  After
than, the usual Rust commands should work.

```
$ nix run gitlab:scvalex/sixty-two  # run without cloning the repo
```

```
$ nix develop  # enter shell with all the deps and variables setup
$ cargo build  # in the develop shell
$ cargo run    # in the develop shell
```

```
$ nix build  # without entering the develop shell (slow)
$ nix run    # without entering the develop shell (slow)
```

Note that the `nixpkgs` version in the flake **must** match the
version you have in NixOS.  I.e. the `nixpkgs.url =
"github:NixOS/nixpkgs/nixos-21.11";` line in the flake must contain
the same version as your running NixOS installation.  If this isn't
the case, you'll likely get an error like "Could not create EGL
display object" with no other details.
