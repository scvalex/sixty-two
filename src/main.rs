use eframe::{epaint::Color32, epi};
use egui::{self, Label, ScrollArea, TextEdit};
use parking_lot::Mutex;
use std::{
    collections::{HashMap, HashSet},
    mem,
    sync::Arc,
    thread,
    time::{Duration, Instant},
};

type OrError<T> = Result<T, anyhow::Error>;
const PRICE_REFRESH_RATE: Duration = Duration::from_secs(10);

fn main() -> OrError<()> {
    let native_options = eframe::NativeOptions {
        initial_window_size: Some((400.0, 200.0).into()),
        ..eframe::NativeOptions::default()
    };
    let app = App::new();
    eframe::run_native(Box::new(app), native_options);
}

struct App {
    state: Arc<Mutex<State>>,
    new_symbol: String,
    new_currency: String,
}

struct State {
    last_updated: Instant,
    prices: HashMap<String, HashMap<String, f64>>,
    symbols: HashSet<String>,
    currency: String,
    frame: Option<epi::Frame>,
    error: Option<String>,
    refresh_requested: bool,
}

impl App {
    fn new() -> Self {
        let state = Arc::new(Mutex::new(State {
            last_updated: Instant::now(),
            prices: HashMap::new(),
            symbols: ["USDT".to_string(), "UST".to_string()].into(),
            currency: "USD".to_string(),
            frame: None,
            error: None,
            refresh_requested: true,
        }));
        thread::spawn({
            let state = Arc::clone(&state);
            move || loop {
                let now = Instant::now();
                let (symbols, currency, last_updated, refresh_requested) = {
                    // We release this lock before doing the network
                    // request in `get_prices`.
                    let mut state = state.lock();
                    (
                        state.symbols.clone(),
                        state.currency.clone(),
                        state.last_updated,
                        mem::replace(&mut state.refresh_requested, false),
                    )
                };
                if refresh_requested
                    || now.saturating_duration_since(last_updated) > PRICE_REFRESH_RATE
                {
                    match get_prices(symbols, currency) {
                        Ok(new_prices) => {
                            let mut state = state.lock();
                            state.last_updated = now;
                            state.prices = new_prices;
                            state.error = None;
                        }
                        Err(err) => state.lock().error = Some(err.to_string()),
                    }
                }
                {
                    // Trigger a repaint every second.
                    state.lock().repaint();
                }
                thread::sleep(Duration::from_secs(1));
            }
        });
        Self {
            state,
            new_symbol: String::new(),
            new_currency: String::new(),
        }
    }
}

impl State {
    fn repaint(&self) {
        if let Some(frame) = self.frame.as_ref() {
            frame.request_repaint();
        }
    }
}

impl epi::App for App {
    fn name(&self) -> &str {
        "Sixty-Two"
    }

    fn setup(
        &mut self,
        _ctx: &egui::Context,
        frame: &epi::Frame,
        _storage: Option<&dyn epi::Storage>,
    ) {
        self.state.lock().frame = Some(frame.clone());
    }

    fn update(&mut self, ctx: &egui::Context, _frame: &epi::Frame) {
        let mut state = self.state.lock();
        egui::CentralPanel::default().show(ctx, |ui| {
            ScrollArea::both().show(ui, |ui| {
                ui.vertical_centered(|ui| {
                    ui.horizontal(|ui| {
                        ui.label("Prices in ");
                        let currency_resp = ui.add(
                            TextEdit::singleline(&mut self.new_currency)
                                .hint_text("USD")
                                .desired_width(200.0),
                        );
                        if !self.new_currency.is_empty()
                            && (ui.button("Change").clicked()
                                || (currency_resp.lost_focus()
                                    && ui.input().key_pressed(egui::Key::Enter)))
                        {
                            state.currency = mem::take(&mut self.new_currency);
                            state.refresh_requested = true;
                        }
                    });
                    ui.label(format!(
                        "Updated {:.2}s ago",
                        state.last_updated.elapsed().as_secs()
                    ));
                    if let Some(error) = state.error.as_ref() {
                        ui.label(
                            egui::RichText::new(error)
                                .size(32.0)
                                .background_color(Color32::RED)
                                .color(Color32::WHITE),
                        );
                    }
                    let mut prices: Vec<(String, String, f64)> = state
                        .prices
                        .iter()
                        .flat_map(|(symbol, prices)| {
                            prices
                                .iter()
                                .map(|(currency, price)| {
                                    (symbol.to_string(), currency.to_string(), *price)
                                })
                                .collect::<Vec<_>>()
                        })
                        .collect();
                    prices.sort_by(|(c1, s1, _), (c2, s2, _)| (c1, s1).cmp(&(c2, s2)));
                    for (symbol, currency, price) in prices {
                        ui.horizontal(|ui| {
                            ui.add(
                                Label::new(
                                    egui::RichText::new(format!(
                                        "1 {:>4} = {:8.2} {}",
                                        symbol, price, currency
                                    ))
                                    .size(32.0)
                                    .monospace(),
                                )
                                .wrap(false),
                            );
                            if ui.button("X").clicked() {
                                state.symbols.remove(&symbol);
                                state.refresh_requested = true;
                            }
                        });
                    }
                    ui.horizontal(|ui| {
                        ui.label("Add symbol ");
                        let symbol_resp = ui.add(
                            TextEdit::singleline(&mut self.new_symbol)
                                .desired_width(100.0)
                                .hint_text("BTC"),
                        );
                        if !self.new_symbol.is_empty()
                            && (ui.button("Add").clicked()
                                || (symbol_resp.lost_focus()
                                    && ui.input().key_pressed(egui::Key::Enter)))
                        {
                            state.symbols.insert(mem::take(&mut self.new_symbol));
                            state.refresh_requested = true;
                        }
                    });
                });
            });
        });
    }
}

fn get_prices(
    symbols: HashSet<String>,
    currency: String,
) -> OrError<HashMap<String, HashMap<String, f64>>> {
    let text = {
        ureq::get(&format!(
            "https://min-api.cryptocompare.com/data/pricemulti?fsyms={}&tsyms={}",
            symbols
                .into_iter()
                .collect::<Vec<String>>()
                .as_slice()
                .join(","),
            currency
        ))
        .call()?
        .into_string()?
    };
    Ok(serde_json::from_str(&text)?)
}
